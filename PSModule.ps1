﻿# @Original Author : Anand Gundabatula
# Purpose          : RemoteSSH - to execute on remote server and execute tasks
# Date             : 28-Jun-2018
# Comments         : A powershell library having common UDF's

Write-Host "The powershell module is loading..." -ForegroundColor Green

# Get-Cred         : To read user credentials with domain
# log              : logging user data
# Read-Config      : Helps to read configuration file and store data as key value pair
# Pre-Check        : Helps to validate pre-requisites before script execution
# Read-Cred        : To read user input credentails
# Get-TimeStamp    : For time stamp
# Set-Title        : To set title of script execution window
# MessageBox-Input : To handle popup and dialog windows to accept and close

function Get-Cred{
    Write-Host "";
    $username = Read-Host "Enter username (no domain required)"
    if ($username -notlike "FNFIS\*"){$username = "FNFIS\$username"}
    Write-Host ""
    $password = Read-Host  -AsSecureString "Password to access"
    $credential = New-Object System.Management.Automation.PSCredential($username,$password)
    return $credential
}

function log($string, $color){
    if ($color -eq $null) {$color = "white"}
    Write-host $string -ForegroundColor $color
    $string | Out-File -FilePath $logfile -append
}

function Read-Config($currentDirectory){
    Get-Content $currentDirectory"\configuration.properties" | foreach-object -begin {$h=@{}} -process { 
    $k = [regex]::split($_,'='); 
        if(($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) 
        { 
                $h.Add($k[0], $k[1]) 
        } 
    }
    return $h
}

function Pre-Check(){
    # Checking Pre-requisites
    if  ( ( (Get-WmiObject win32_operatingsystem).Name -match "Windows 7" ) -and 
            ( (Get-ItemProperty "HKLM:Software\Microsoft\NET Framework Setup\NDP\v4\Full").Version -gt 4.7) -and ((Get-PSSnapin).PSVersion.Major -ge 5) ) {
        Write-Host (Get-WmiObject win32_operatingsystem).Name
        Write-Host ".Net Framework version"(Get-ItemProperty "HKLM:Software\Microsoft\NET Framework Setup\NDP\v4\Full").Version
        Write-Host "Installed PS Version"(Get-PSSnapin).PSVersion.Major
    }else {
        Write-Host  (Get-WmiObject win32_operatingsystem).Name ",Please install PS version \\atlwspe05.fisdev.local\Projects_E_Drive\Profile 7.6.2\Demo\Win7AndW2K8R2-KB3191566-x64"
        exit
    }

    # Module Check
    if ( (Get-Module -All).Name -eq 'PoshSSH' ) {
        Write-Host -BackgroundColor DarkGreen "POSH-SSH module already installed!"
    }else {
        #Set-ExecutionPolicy -ExecutionPolicy AllSigned
        Install-Module -Name Posh-SSH -RequiredVersion 2.0.2
    }
}

function Read-Cred(){
    Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\PowerShell\1\ShellIds" -Name "ConsolePrompting" -Value $True
    $User = read-host 'Enter Username' 
    $PWord = read-host 'Enter Password' -AsSecureString ;
    $psCredential = New-Object -TypeName "System.Management.Automation.PSCredential" -ArgumentList $User, $PWord
    return $psCredential
}

function Get-TimeStamp(){
    return $((Get-Date).ToString('yyyy-MM-dd-HHmm'))
}

function Set-Title(){
    $host.ui.RawUI.WindowTitle = "SPE - Remote SSH"
}

#Usage: 
#1. MessageBox-Input 'Print','escape'
#2. MessageBox-Input "Delete Files" "YES"
function MessageBox-Input($dialogTitle, $action){
    [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 
    $wshell = New-Object -ComObject wscript.shell;
    $wshell.AppActivate($dialogTitle)
    Start-Sleep -seconds 3
    if ( ($action -ieq 'print') -or (($action -ieq 'ok') -or ($action -ieq 'yes')) ) { 
        [System.Windows.Forms.SendKeys]::SendWait('{ENTER}')
    }elseif (($action -ieq 'no') -or ($action -ieq 'cancel')) {
        [System.Windows.Forms.SendKeys]::SendWait('{TAB}')
        [System.Windows.Forms.SendKeys]::SendWait('{ENTER}')
    }elseif ($action -ieq 'escape'){
        [System.Windows.Forms.SendKeys]::SendWait('{ESC}')
    }
}