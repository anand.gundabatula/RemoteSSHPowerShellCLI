#!/bin/bash


############################   ########################################################

#whilele IFS='=' read -r key val; do
 #       case $key in
  #           ''|\#*) continue ;;
   #     esac
    #    echo "key1: '$key'"

 #       [[ $key = '#'* ]] && continue
  #      array["$key"]="$val"            #bash4
   #     keys+=("$key"); vals+=("$val")  #bash3
#done < "${ENV}.properties"

ENV=${1:-ev}

function prop {

    grep "${1}" ${ENV}.properties|cut -d'=' -f2
}
echo "####################################################################################################################"

echo $(prop 'GlobalFlag')
globalflag=$(prop 'GlobalFlag')

echo $(prop 'Global.Resultlog.path')
gloabllog=$(prop 'Global.Resultlog.path')

echo "globalflag=$globalflag"
echo "gloabllog=$gloabllog"

d=$(grep "^Instance\([0-9]\+\)=" ${ENV}.properties | wc -l)
echo "count=$d"
echo "#####################################################################################################################"
#count=$d
 #for count in $(seq 1 $d)

#do
#for 
if [[ $globalflag = 'y' ]] | [[ $globalflag = 'Y' ]]
then
for counter in $(seq 1 $d)
do
instancepath=$(prop 'Instance'$counter'=')
shutdown=$(prop 'Instance'$counter'.shutdown')
startup=$(prop 'Instance'$counter'.startup')
logpathcount=$(grep "^Instance"$counter".logs\(.*\)=" ${ENV}.properties | wc -l)
#logspathlogcount=$(grep "^Instance"$counter"logspath\([0-9]\+\)log=" ${ENV}.properties | wc -l)
echo "logspathcount=$logpathcount"
#echo "logspathlogcount=$logspathlogcount"

echo "-------------------------------------------------------------------------------------------------------"

echo "Instnacepath=$instancepath"

#--------------------------------------------------------Instance Shutdown Start ------------------------------------------
#cd $instancepath

$(echo "sh $instancepath/$shutdown")
#--------------------------------------------------------Instance Shitdown Stop--------------------------------------------


while IFS='=' read -r key val; do
        case $key in
            ''|Global*|shutdown*|logs|startup*|\#*) continue ;;
        esac
       # echo "key1: '$key'"
#	echo "$key = $val"
#echo "Instance"$count"shutdown"
if [[ $key =~ "Instance$counter.prop." ]]
then
echo "$key"
echo "instance=$counter"
#echo $(prop $key)
#data=$(prop ^$key)
data=$val
echo "data=$val"
counters=$(echo $data | grep -o "#" | wc -l)
((counters=$counters+1))
echo "counter=$counters"

#echo "$data"
#echo "globalflag=$globalflag"
#echo "gloabllog=$gloabllog"

c=0
#for i in ${data//#/ }
for i in $(seq 1 $counters)
do
array[$c]=$(echo ${data} | cut -d"#" -f$i)
c=$c+1
done

filepath=${array[0]}
propertytagname=${array[1]}
propertytvalue=${array[2]}
IndividualLineFlag=${array[3]}
MultipleReplacementFlag=${array[4]}
SamelineFlag=${array[5]}
SingleLineMultiplePropertiesTag=${array[6]}
copyfilename=${array[7]}
#echo "path=$filepath"
#echo "name=$propertytagname"
#echo "value=$propertytvalue"
#echo "indiline=$IndividualLineFlag"
#echo "multireplace=$MultipleReplacementFlag"
#echo "SamelineFlag=$SamelineFlag"
#echo "singlelinemulti=$SingleLineMultiplePropertiesTag"
#echo "copyfilename=$copyfilename"
echo "--------------------------------------------------------------------------------------------------------------------"

if [  -f $filepath ]
then 
echo "file exists"

split1=$(echo ${filepath} | cut -d"." -f1)
split2=$(echo ${filepath} | cut -d"." -f2)
copypath="$(echo "$split1")-ORG.$split2-$(date +"%d-%b-%Y")"

#echo "split1=$split1"
#echo "split2=$split2"
cp $filepath $copypath

if [[ $filepath =~ ".properties" ]]
then

echo "properties"

if [ $IndividualLineFlag = y ] | [ $IndividualLineFlag = Y ]
then
echo "indi"
count=$(grep -c ^$propertytagname\= $filepath)
#echo "count=$count"
if [ $count -lt "1" ]

then

echo "no where found"
 echo "$propertytagname=$propertytvalue" >> $filepath
	echo "$(date +"%d-%b-%Y")_Property$propertytagname=$propertytvalue was not there and added now" >>$gloabllog
else 
echo "found"
sed -i '/^'"$propertytagname"'=/c\'"$propertytagname"'='"$propertytvalue"'' $filepath
echo "$(date +"%d-%b-%Y")_Already Property $propertytagname  exists and updated its value with $propertytvalue " >>$gloabllog
fi 
elif [ $MultipleReplacementFlag = y ] | [ $MultipleReplacementFlag = Y ]
then
count=$(grep -c $propertytagname\= $filepath)
#echo "$count"

sed -i -e 's/'"$propertytagname"'=\([0-9a-zA-Z]\+\)/'"$propertytagname"'='"$propertytvalue"'/g'  $filepath

echo "no where found"
echo "$(date +"%d-%b-%Y")_multiple times occuring property $propertytagname's value replaced with $propertytvalue" >>$gloabllog
#else
#break
#fi
elif [ $SamelineFlag = y ] | [ $SamelineFlag = Y ]
then

echo "sameline"
MultiplePropertiesTagline=$(sed -n '/'"$SingleLineMultiplePropertiesTag"'\=/p' $filepath)
if [[ $MultiplePropertiesTagline =~ $propertytagname ]]
then
sed -i -e '/^'"$SingleLineMultiplePropertiesTag"'=/ s/'"$propertytagname"'=\([0-9a-zA-Z]\+\)/'"$propertytagname"'='"$propertytvalue"'/g' $filepath
echo "$(date +"%d-%b-%Y")_Property $propertytagname present in Property $SingleLineMultiplePropertiesTag is updated with $propertytvalue" >>$gloabllog
else
sed -i '/^'"$SingleLineMultiplePropertiesTag"'=/ s/$/\|'"$propertytagname"'='"$propertytvalue"'/' $filepath
echo "$(date +"%d-%b-%Y")_Property $propertytagname was not present in Property $SingleLineMultiplePropertiesTag now added with value $propertytvalue" >>$gloabllog
fi 
 
else
echo "Error"

#d=0
for i in ${propertytagname//,/ }
do
#source[$d]=$i
#d=$d+1
sed -i -e 's/'"$i"'/'"$propertytvalue"'/g' $filepath
echo "$(date +"%d-%b-%Y")_Replaced $i with propertytvalue" >>$gloabllog
done

fi
elif [[ $filepath =~ ".xml" ]]
then

echo "xml"
if [ $IndividualLineFlag = y ] | [ $IndividualLineFlag = Y ]
then
echo "indi"
#count=$(grep -c \^\t\<$propertytagname $filepath)
outertag=$(sed -n '/^    <'"$propertytagname"'\(.*\+\)\/>/p' $filepath)
echo "count=$outertag"
#if grep -qF "$propertytagname" "$filepath"
#then
#echo "yes we have"
#fi
outertagsplit1=$(echo ${propertytvalue} | cut -d"," -f1)
outertagsplit=$(echo ${propertytvalue} | cut -d"," -f2)

echo "outertagsplit1=$outertagsplit1"
echo "outertagsplit=$outertagsplit"


lines=$(sed -ne '/^    <'"$propertytagname"'/,/\/>/p' $filepath)
linenumber=$(grep -n "^    <"$propertytagname"" $filepath | cut -d : -f 1)
echo "lines=$lines"
echo "linenumner=$linenumber"
totallines=$(wc -l $filepath | cut -d" " -f1)
echo "totallines=$totallines"
if [[ $lines =~ $outertagsplit1 ]]
then
echo "inside If"
for (( k=$linenumber; k<$totallines ; k++ ));
#for k in $(seq $linenumber $totallines)
do
echo "inside for"
linecontent=$(sed -n ''"$k"'p'  $filepath)
if [[ $linecontent =~ $outertagsplit1 ]]
then
echo "$k"
echo "linecontent=$linecontent"
sed -i ''"$k"'s/'"$outertagsplit1"'="\([0-9a-zA-Z\/%\.]\+\)"/'"$outertagsplit1"'="'"$outertagsplit"'"/' $filepath
echo "$(date +"%d-%b-%Y")_Already Property $propertytagname  exists and updated its value with $outertagsplit1 = $outertagsplit " >>$gloabllog
break
fi

done
fi
elif [ $MultipleReplacementFlag = y ] | [ $MultipleReplacementFlag = Y ]
then
count=$(grep -c $propertytagname\= $filepath)
#echo "$count"

sed -i -e 's/'"$propertytagname"'="\([0-9a-zA-Z\/%\.]\+\)"/'"$propertytagname"'="'"$propertytvalue"'"/g'  $filepath

echo "no where found"
echo "$(date +"%d-%b-%Y")_multiple times occuring property $propertytagname's value replaced with $propertytvalue" >>$gloabllog
elif [ $SamelineFlag = y ] | [ $SamelineFlag = Y ]
then

echo "sameline"
#MultiplePropertiesTaigline=$(sed -n '/'"$SingleLineMultiplePropertiesTag"'\=/p' $filepath)

outertagsplit1=$(echo ${SingleLineMultiplePropertiesTag} | cut -d"," -f1)
outertagsplit=$(echo ${SingleLineMultiplePropertiesTag} | cut -d"," -f2)

echo "outertagsplit1=$outertagsplit1"
echo "outertagsplit=$outertagsplit"
tagcounters=$(echo $propertytvalue | grep -o "," | wc -l)
x=0
for i in $(seq 1 $tagcounters)
do
xmlarray[$x]=$(echo ${propertytvalue} | cut -d"," -f$i)
x=$x+1
done
tags=$(echo ${propertytvalue} | cut -d"," -f1)

#lines=$(sed -ne '/^    <'"$propertytagname"'/,/\/>/p' $filepath)
linenumber=$(grep -n "<$outertagsplit1>"$outertagsplit"" $filepath | cut -d : -f 1)
echo "linenumber=$linenumber"
for (( i=$linenumber; i<500 ; i++ ))
do
linecontent=$(sed -n ''"$i"'p'  $filepath)
if [[ $linecontent =~ $propertytagname ]]
then
echo "$i"
echo "$linecontent"
sed -i ''"$i"'s/'"$outertagsplit1"'="\([0-9a-zA-Z\/%\.]\+\)"/'"$outertagsplit1"'="'"$outertagsplit"'"/' $filepath
break
fi
done
if [[ $lines =~ $propertytagname ]]
then
if [[ $lines =~ $outertagsplit1 ]]
then


echo "found"

sed -i -e '/^    <'"$propertytagname"'/ s/'"$outertagsplit1"'="\([0-9a-zA-Z\/%\.]\+\)"/'"$outertagsplit1"'="'"$outertagsplit"'"/g' $filepath
echo "after"
echo "$(date +"%d-%b-%Y")_Already Property $propertytagname  exists and updated its value with $outertagsplit1 = $outertagsplit " >>$gloabllog
fi
fi


if [[ $MultiplePropertiesTagline =~ $propertytagname ]]
then
sed -i -e '/^'"$SingleLineMultiplePropertiesTag"'=/ s/'"$propertytagname"':\([0-9a-zA-Z]\+\)/'"$propertytagname"':'"$propertytvalue"'/g' $filepath
echo "$(date +"%d-%b-%Y")_Property $propertytagname present in Property $SingleLineMultiplePropertiesTag is updated with $propertytvalue" >>$gloabllog
else
sed -i '/^'"$SingleLineMultiplePropertiesTag"'=/ s/$/\ '"$propertytagname"':'"$propertytvalue"'/' $filepath
echo "$(date +"%d-%b-%Y")_Property $propertytagname was not present in Property $SingleLineMultiplePropertiesTag now added with value $propertytvalue" >>$gloabllog
fi
else
echo "Error"

#d=0
for i in ${propertytagname//,/ }
do
#source[$d]=$i
#d=$d+1
sed -i -e 's/'"$i"'/'"$propertytvalue"'/g' $filepath
echo "$(date +"%d-%b-%Y")_Replaced $i with propertytvalue" >>$gloabllog
done

fi

else 

echo "sh"
if [ $IndividualLineFlag = y ] | [ $IndividualLineFlag = Y ]
then
echo "indi"
count=$(grep -c ^$propertytagname\= $filepath)
#echo "count=$count"
if [ $count -lt "1" ]

then

echo "no where found"
 echo "$propertytagname=\"$propertytvalue\"" >> $filepath
        echo "$(date +"%d-%b-%Y")_Property$propertytagname=$propertytvalue was not there and added now" >>$gloabllog
else
echo "found"
sed -i '/^'"$propertytagname"'=/c\'"$propertytagname"'="'"$propertytvalue"'"' $filepath
echo "$(date +"%d-%b-%Y")_Already Property $propertytagname  exists and updated its value with $propertytvalue " >>$gloabllog
fi
elif [ $MultipleReplacementFlag = y ] | [ $MultipleReplacementFlag = Y ]
then
count=$(grep -c $propertytagname\= $filepath)
#echo "$count"

sed -i -e 's/'"$propertytagname"'=\([0-9a-zA-Z]\+\)/'"$propertytagname"'='"$propertytvalue"'/g'  $filepath

echo "no where found"
echo "$(date +"%d-%b-%Y")_multiple times occuring property $propertytagname's value replaced with $propertytvalue" >>$gloabllog
#else
#break
#fi
elif [ $SamelineFlag = y ] | [ $SamelineFlag = Y ]
then

echo "sameline"
MultiplePropertiesTagline=$(sed -n '/'"$SingleLineMultiplePropertiesTag"'\=/p' $filepath)
if [[ $MultiplePropertiesTagline =~ $propertytagname ]]
then
sed -i -e '/^'"$SingleLineMultiplePropertiesTag"'=/ s/'"$propertytagname"':\([0-9a-zA-Z]\+\)/'"$propertytagname"':'"$propertytvalue"'/g' $filepath
echo "$(date +"%d-%b-%Y")_Property $propertytagname present in Property $SingleLineMultiplePropertiesTag is updated with $propertytvalue" >>$gloabllog
else
sed -i '/^'"$SingleLineMultiplePropertiesTag"'=/ s/$/\ '"$propertytagname"':'"$propertytvalue"'/' $filepath
echo "$(date +"%d-%b-%Y")_Property $propertytagname was not present in Property $SingleLineMultiplePropertiesTag now added with value $propertytvalue" >>$gloabllog
fi
else
echo "Error"

#d=0
for i in ${propertytagname//,/ }
do
#source[$d]=$i
#d=$d+1
sed -i -e 's/'"$i"'/'"$propertytvalue"'/g' $filepath
echo "$(date +"%d-%b-%Y")_Replaced $i with propertytvalue" >>$gloabllog
done

fi

fi
else


#chmod 777 $loggingpath

split3=$(echo ${filepath} | cut -d"." -f1)
split4=$(echo ${filepath} | cut -d"." -f2)

cp $filepath $(echo "$split3/"$copyfilename"."$split4"")


echo "file has added"
fi
fi
done < "${ENV}.properties"

while IFS='=' read -r key val; do
        case $key in
            ''|Global*|shutdown*|startup*|\#*) continue ;;
        esac
#echo "logs $key : $val"
if [[ $key =~ "Instance"$counter".logs" ]]

then
logdata=$val
#echo "logs=$val"
logpaths=$(echo ${logdata} | cut -d"#" -f1)
lognames=$(echo ${logdata} | cut -d"#" -f2)
#echo "$logpaths:$lognames"

#-----------------------------------------------------------------Logs Removal--------------------------------------------------------------------
#cd $logpaths
#for m in ${lognames//,/ }
#do
#rm $instancelogpathlog
#done
#---------------------------
fi
done < "${ENV}.properties"



#----------------------------------------------------------------- Instance Restart Start -------------------------------------------------------------
#cd $instancepath
#sh ./startup.sh
$(echo "sh $instancepath/$startup")
#----------------------------------------------------------------- Instance Restart Stop -------------------------------------------------------------
done
else
echo "No Properties have been updated" >>$gloabllog

fi


