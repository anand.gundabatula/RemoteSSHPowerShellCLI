﻿# @Original Author      : Anand Gundabatula
# Purpose               : RemoteSSH - to execute on remote server and execute tasks
# Date                  : 28-Jun-2018
# Dependencies          : Posh-SSH Module

#Set-ExecutionPolicy -ExecutionPolicy AllSigned -Confirm
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Confirm

Set-Title
Clear-Host
#Start-Transcript -path "MyTranscript.txt"
$MyInvocation = (Get-Variable MyInvocation).Value
$currentDirectory = Split-Path -Parent $MyInvocation.MyCommand.Path

Import-Module -Force $currentDirectory\PSModule.ps1
#$env:PSModulePath=$env:PSModulePath + ";" + $currentDirectory +"\PSModule.ps1"
$h = Read-Config $currentDirectory
Pre-Check

$downloadDirectory = [IO.Path]::GetDirectoryName($currentDirectory + '\download\')
$remoteDirectory = '/tmp/'

# Enter User Credentials
$psCredential = Read-Cred

$devicelist = [IO.Path]::Combine($currentDirectory, 'devices.csv')
$devices = Import-Csv -path $devicelist -header 'ip','hostname','appType','userType','cfgFile','logsflag','configflag' -delimiter ','
#$currentHost=""
foreach ($device in $devices)
{
    #Skip Header
    if (($device.ip -eq "ip") -or ($device.ip -contains ".")) { continue; }

    if ( ([string]::IsNullOrEmpty($device.cfgFile)) ) {
        Write-Host "Config file entry is missing in devices.csv, skipping "$_hostname
        continue;
    }
 
    $session = New-SSHSession -ComputerName $device.ip -Credential $psCredential
    $sshshellstream = New-SSHShellStream -Index $session.SessionId
    if ($device.logsflag -ieq "y") {
        
        # Enter Test Run ID
		Clear-Host
        $RunId = Read-Host 'Enter Run ID'

        $rmFlag=""
        $confirm = New-Object -comobject wscript.shell
        $intChoice = $confirm.popup("Do you want to cleanup logs?",0,"Clear logs",4) 
        if ($intChoice -eq 6) { $rmFlag='y' }
        
        $dataDirectory = [IO.Path]::GetDirectoryName($currentDirectory + '\data\')
        Set-SCPFolder -LocalFolder $dataDirectory -RemoteFolder $remoteDirectory -ComputerName $device.ip -Credential $psCredential -AcceptKey
        
        $_hostname = $device.hostname.Split('.')[0]
        $_tarFile = $RunId + "_" + $_hostname + "_" + $device.appType + "_" +$device.userType + ".tar"

#Step-1 : Logon as sudo user and execute script
$sshshellstream.WriteLine('sudo su - ' + $device.userType + ' << block 
cp /tmp/logsbkpclean.sh /usr/tmp;
cp /tmp/' + $device.cfgFile + ' /usr/tmp;
cd /usr/tmp;
chmod 777 logsbkpclean.sh;
./logsbkpclean.sh ' + $device.cfgFile + ' ' + $_tarFile + ' ' + $rmFlag +';
rm -f logsbkpclean.sh ' + $device.cfgFile + ';
exit;
block
')

        Start-Sleep -milliseconds 5000
        $sshshellstream.Read()

$_tarFile = $_tarFile + ".gz"
#Step-2 : Copy tar under normal user
$sshshellstream.WriteLine('
cp /usr/tmp/' + $_tarFile + ' /tmp/;
cd /tmp/
rm -f ' + $device.cfgFile + ' logsbkpclean.sh;
')

        Start-Sleep -milliseconds 5000
        $sshshellstream.Read()

#Step-3 : Remove tar file
$sshshellstream.WriteLine('sudo su - ' + $device.userType + ' << block 
cd /usr/tmp
rm -f ' + $_tarFile + ';
exit;
block
')

        Start-Sleep -milliseconds 5000
        $sshshellstream.Read()

        # Copy tar file to local
        $remotePath = $remoteDirectory+$_tarFile
        $LocalPath = $downloadDirectory+'\'+$_tarFile
        start-sleep -milliseconds 5000
        try{
            Get-SCPFolder -LocalFolder $downloadDirectory -RemoteFolder $remotePath -ComputerName $device.ip -Credential $psCredential -ErrorAction Stop -AcceptKey
        }catch {
            Write-Host -BackgroundColor DarkRed $_.Exception.Message
        }

        #Step-4 : Cleanup /tmp folder
$sshshellstream.WriteLine('
cd /tmp/
rm -f ' + $_tarFile + ';
')

        Start-Sleep -milliseconds 5000
        $sshshellstream.Read()

        try {
            $_File = Get-ChildItem $LocalPath | Where {$_.LastWriteTime -gt (Get-Date).AddHours(-24)} -ErrorAction Stop
            if ($_File.Exists) {
                $dialog = New-Object -comobject wscript.shell
                
                $intAnswer = $dialog.popup("Do you want to upload locally downloaded files to shared drive?",0,"Upload Files",4) 
                if ($intAnswer -eq 6) { Copy-Item $_File $h.Get_Item("SharedDrive")  }
                
                $intAnswer = $dialog.popup("Do you want to delete locally downloaded files?",0,"Delete Files",4) 
                if ($intAnswer -eq 6) { Remove-Item -Path $_File }
            }else { Write-Host -BackgroundColor DarkRed "Unable to upload file into shared drive!. Please check..." }
        }catch {
            Write-Host -BackgroundColor DarkRed $_.Exception.Message
        }
    }


    if ($device.configflag -ieq "y") {
        Clear-Host
        $dataDirectory = [IO.Path]::GetDirectoryName($currentDirectory + '\conf\')
        Set-SCPFolder -LocalFolder $dataDirectory -RemoteFolder $remoteDirectory -ComputerName $device.ip -Credential $psCredential

        Write-Host "Step : Configuration update "$device.userType
        Start-Sleep -milliseconds 5000

#Step-1 : Logon as sudo user and execute script
$sshshellstream.WriteLine('sudo su - ' + $device.userType + ' << block 
ls -ltr;
cp /tmp/configupdate.sh /usr/tmp;
cp /tmp/config.properties /usr/tmp;
cd /usr/tmp;
chmod 777 configupdate.sh;
./configupdate.sh config;
rm -f configupdate.sh config.properties;
exit;
block
')
        Start-Sleep -milliseconds 5000
        $sshshellstream.Read()


#Step-1 : Logon as sudo user and execute script
$sshshellstream.WriteLine('
cd /tmp/;
rm -f configupdate.sh config.properties;
')
        Start-Sleep -milliseconds 5000
        $sshshellstream.Read()


    }
    
    # Closing and disconnecting all sessions
    $sshshellstream.Dispose()
    $sshshellstream.Close()
    $session.Disconnect()

    # Removing SSH Session
    Remove-SSHSession -Index $session.SessionId

}

#Stop-Transcript