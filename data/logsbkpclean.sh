#!/bin/bash

# @Author Anand
# @Purpose To take logs backup based on user config file and cleanup based on user inputs
# @Creation Date 15-May-2018

_now=$(date +%Y%m%d_%H%M%S)

#read -p "Do you wish to take logs backup? (y/n)" logsbkp
#read -p "Do you wish to delete/clean log files? (y/n)" removelogs

#BACKUP_FILENAME="Logsbackup"_"$_now"
servername=$(hostname -s)
BACKUP_FILENAME=$2
rmFlg=$3
if [ ! -z $1 ]
then 
    : # $1 was given
	LOGCONFIG=./$1
	echo "optional param passed"
else
    : # $1 was not given
	LOGCONFIG=./$servername.config
	echo "default config"
fi

# Check if config file exists else exit
if [[ -f $LOGCONFIG ]]; then
	echo "Config file found, logs backup started..."
	touch empty.tmp
	if [[ -f empty.tmp ]]; then
		tar -cvf $BACKUP_FILENAME ./empty.tmp
	
		echo "Creating tar file completed."
		while read -r line; do
		#printf "\n%s\n" "$line"
		if [ -d "$line" ]; then
			DIR=$line
		else
			tar -uvf $BACKUP_FILENAME -P $(echo $DIR/$line)
		fi
		done < $LOGCONFIG

		# Remove .tmp file from the tar file 
		tar --wildcards --delete -f $BACKUP_FILENAME ./empty.tmp
		rm -f ./empty.tmp
	
		echo "Logs backup completed."
		
		# NMON file backup
		# @Updated on 06-Jul-2018
		_path=$(ps -ef|grep nmon|awk '{print $11}')
		if [[ "$_path" == *"/opt/wily/nmon/"* ]]; then  
			nmon_path=$(echo "/opt/wily/nmon/nmon_data")
		elif [[ "$_path" == *"/profile/nmon/"* ]]; then
			nmon_path=$(echo "/profile/nmon/nmon_data")
		elif [[ "$_path" == *"/usr/local/nmon/"* ]]; then
			nmon_path=$(echo "/usr/local/nmon/nmon_data")
		else
			echo "nmon file not found!!!"
		fi
		
		if [ -d "$nmon_path" ]; then
			tar -uvf $BACKUP_FILENAME -P $(echo $nmon_path/*.nmon)
		fi
			
		gzip $BACKUP_FILENAME
	else
		echo 'tar file not created successfully, failed at first step!!!'
		exit
	fi
else
	echo "$LOGCONFIG not found!!!"
	exit
fi

if [[ $rmFlg == 'y' ]]; then
        echo "remove logs set to y"
	while read -r line; do
		#printf "\n%s\n" "$line"
		if [ -d "$line" ]; then
			DIR=$line
		else
			rm -f $(echo $DIR/$line)
		fi
	done < $LOGCONFIG
fi


